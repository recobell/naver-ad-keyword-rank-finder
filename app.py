__author__ = 'jackyun'
from flask import Flask, request, render_template
app = Flask(__name__)
import requests as r
from bs4 import BeautifulSoup as bs


NAVER_SEARCH_URL = 'https://search.naver.com/search.naver?where=nexearch&sm=top_hty&fbm=1&ie=utf8&query={}'


@app.route("/")
def hello():
    return render_template('main.html')


@app.route("/rank_finder", methods=['POST'])
def rank_finder():
    result = []
    if request.method == 'POST':
        file = request.files['file']
        result = extract_rank(file)
    return render_template('keyword.html', result=result)


def extract_rank(file):
    result = []
    for line in file.stream.read().decode('utf8').split('\n'):
        keyword, url = line.split(',')
        try:
            pl_rank, bz_rank = get_ranking_from_naver(keyword, url)
            result.append({"keyword": keyword, "url": url, "pl_rank": pl_rank, "bz_rank": bz_rank})
        except Exception as e:
            print(e)
            result.append({"keyword": keyword, "url": url, "pl_rank": "error", "bz_rank": "error"})
    return result


def get_ranking_from_naver(keyword, url):
    tmp_bs =bs(r.get(NAVER_SEARCH_URL.format(keyword)).text, 'html.parser')
    try:
        pl_rank = get_pl_section_rank(tmp_bs, url)
    except:
        pl_rank = 'no rank'
    try:
        bz_rank = get_bz_section_rank(tmp_bs, url)
    except:
        bz_rank = 'no rank'
    return pl_rank, bz_rank


def get_pl_section_rank(bs_object, url):
    candidates = [line.text for line in bs_object.select('.ad_section._pl_section')[0].select('a.lnk_url')]
    rank = get_list_rank(candidates, url)
    return rank


def get_bz_section_rank(bs_object, url):
    candidates = [line.text for line in bs_object.select('.ad_section._bz_section')[0].select('a.lnk_url')]
    rank = get_list_rank(candidates, url)
    return rank


def get_list_rank(itemlist, item):
    cnt = 1
    for it in itemlist:
        if it.startswith(item):
            return cnt
        else:
            cnt += 1
    return 'no rank'

if __name__ == "__main__":
    app.run(debug=True)